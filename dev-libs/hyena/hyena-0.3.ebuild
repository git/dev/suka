# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit gnome2 mono

DESCRIPTION="Library behind banshee"
HOMEPAGE="http://live.gnome.org/Hyena"

LICENCES="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"

RDEPEND="dev-dotnet/gtk-sharp:2"

DEPEND="${RDEPEND}
	dev-util/pkgconfig"

