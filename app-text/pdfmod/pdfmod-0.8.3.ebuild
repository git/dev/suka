# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="2"

inherit gnome2 mono

DESCRIPTION="A simple tool for modifying PDF documents"
HOMEPAGE="http://live.gnome.org/PdfMod"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=">=dev-lang/mono-2
	>=dev-dotnet/gtk-sharp-2.0
	>=dev-dotnet/gconf-sharp-2.0
	app-text/gnome-doc-utils
	>=app-text/poppler-0.8.0
	>=dev-libs/hyena-0.3"

RDEPEND="${DEPEND}"

DOCS="AUTHORS NEWS README"
